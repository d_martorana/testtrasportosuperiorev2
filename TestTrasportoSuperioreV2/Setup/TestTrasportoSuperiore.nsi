; File da includere
	!include "MUI2.nsh"

; Dati applicazione
	!define APPNAME "Test Trasporto Superiore"
	Name "${APPNAME}"
	!define COMPANYNAME "ARCA Technologies"
	!define VERSIONMAJOR 2
	!define VERSIONMINOR 2
	!define VERSIONBUILD 1

; Impostazione cartelle di sorgente, output ed installazione
	!define INSTFOLDER "C:\ARCA\TestTrasportoSuperiore"
	!define SOURCEFOLDER "H:\Progetti VBNET\CM18\TestTrasportoSuperioreV2\TestTrasportoSuperioreV2\bin\Debug\"
	OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
	
; Impostazione Icona installazione/disinstallazione
	!define MUI_ICON "H:\Progetti VBNET\NSISimages\icona.ico"
	!define MUI_UNICON "H:\Progetti VBNET\NSISimages\icona.ico"
	
	!define MUI_ABORTWARNING
	
; Impostazione header (bmp 150x57 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define MUI_HEADERIMAGE_BITMAP "H:\Progetti VBNET\NSISimages\headerlogo.bmp"
	!define MUI_HEADERIMAGE
	
; Impostazione pagina di benvenuto (testo ed immagine bmp 164x314 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define txtMessageLine1 "SW guidato per il collaudo e la configurazione del trasporto superiore"
	!define txtMessageLine2 ""
	!define txtMessageLine3 ""
	!define MUI_WELCOMEPAGE_TITLE "Installazione Test Trasporto Superiore"
	!define MUI_WELCOMEPAGE_TEXT "${txtMessageLine1}$\r$\n$\r$\n${txtMessageLine2}$\r$\n$\r$\n${txtMessageLine3}"
	!define MUI_WELCOMEFINISHPAGE_BITMAP "H:\Progetti VBNET\NSISimages\ttswelcome.bmp"
	!insertmacro MUI_PAGE_WELCOME
	
; Impostazione pagina della licenza
	!insertmacro MUI_PAGE_LICENSE "H:\Progetti VBNET\NSISimages\ArcaLicense.txt"
	
; Personalizzazione cartella d'installazione
	;!insertmacro MUI_PAGE_DIRECTORY
	
	
	!insertmacro MUI_PAGE_COMPONENTS
	
; Visualizzazione avanzamento installazione file
	!insertmacro MUI_PAGE_INSTFILES
	
; Impostazione linguaggio d'installazione
	!insertmacro MUI_LANGUAGE "English"
	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Installazione DLL per il visualizzatore del DBFW"
	;!define MUI_FINISHPAGE_TEXT "Installazione DLL terminata correttamente"
	;!insertmacro MUI_PAGE_FINISH
	
; Impostazione conferma di disinstallazione
	!insertmacro MUI_UNPAGE_CONFIRM
	
; Visualizzazione avanzamento disinstallazione file
	!insertmacro MUI_UNPAGE_INSTFILES
	
Section "Test Trasporto Superiore"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\TestTrasportoSuperioreV2.exe"
	File "${SOURCEFOLDER}\Checklist-001.txt"
	File "${SOURCEFOLDER}\CMLink.dll"
	File "${SOURCEFOLDER}\CMTrace.dll"
	File "${SOURCEFOLDER}\CMCommand.dll"
	File "${SOURCEFOLDER}\dllError.ini"
	File "${SOURCEFOLDER}\italiano.ini"
	File "${SOURCEFOLDER}\Setup.ini"
	File "${SOURCEFOLDER}\TestValues.ini"
	File "${SOURCEFOLDER}\TheRCs.ini"
	File "${SOURCEFOLDER}\TheRCsCM14_8.ini"
	File "${SOURCEFOLDER}\TheRCsCM18.ini"
	File "${SOURCEFOLDER}\TheRCsCM18B.ini"
	File "${SOURCEFOLDER}\TheRCsCM18SOLO.ini"
	File "${SOURCEFOLDER}\TheRCsCM18T.ini"
	File "${SOURCEFOLDER}\TheRCsCM20.ini"
	File "${SOURCEFOLDER}\TheRCsCM24_8.ini"
	File "${SOURCEFOLDER}\TheRCsCM24B.ini"
	File "${SOURCEFOLDER}\TheRCsEMU_T.ini"
	File "H:\Progetti VBNET\XmlDll\bin\x86\Debug\XmlDll.dll"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\TestTrasportoSuperioreV2.exe" ""
	
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
	
SectionEnd



Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd